/**
 * The settings for the Activity logger, the class will create a instance of it self and put it in `window.activityLoggerSettings`
 * The logger allows for dynamicly updating of its settings, the settings instance can therefor be updated or replaced with a new instance on the fly 
 * @property {ActivityLoggerModes|string} LoggingMode Gets or sets the log mode {ActivityLoggerModes}
 * @property {string[]} TargetElements gets or sets the target elemets an array of html element tagNames
 * @property {ActivityLoggerActions[]} LoggingActions Sets logging actions.
 * @property {string} LoggingServerUrl Gets or Sets the url to the logging server
 */
class ActivityLoggerSettings {
    constructor() {
        this.loggingMode = ActivityLoggerModes.Console;
        this.targetEls = ["a","button","input"];
        this.loggingActions = [
            ActivityLoggerActions.MouseClick,
            ActivityLoggerActions.PageLoad,
            ActivityLoggerActions.PageLeave]
    }
    /**
     * @ignore
     */
    set LoggingMode(value) {
        this.loggingMode = value;
    }
    /**
     * @ignore 
     */
    get LoggingMode() { return this.loggingMode; }

    /**
     * @ignore 
     */
    set TargetElements(value) {
        this.targetEls = value;
    }
    /**
     * @ignore
     */
    get TargetElements() {
        return this.targetEls;
    }
    /**
     * @ignore
     */
    set LoggingActions(value) {
        this.loggingActions = value;
    }

    /**
     * @ignore
     */
    set LoggingServerUrl(url) {
        this.loggingServer = url;
    }
    /**
     * @ignore
     */
    get LoggingServerUrl() {
        return this.loggingServer;
    }
    /**
     * Add Element to the logger 
     * @param {string} value the tagName of the elemet
     */
    AddTargetElement(value) {
        this.targetEls.push(value);
    }
    /**
     * removes elements from the logger, the elements of this type vill nolonger trigger an activity log
     * @param {string} value the tagName of the element 
     */
    RemoveTargetElement(value) {
        let index = this.targetEls.indexOf(value);
        delete this.targetEls[index];
    }
}

(() => {
    if (!window.activityLoggerSettings)
        window.activityLoggerSettings = new ActivityLoggerSettings();
})();