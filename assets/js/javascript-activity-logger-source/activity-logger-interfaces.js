/**
 * @interface ActivityLogData
 * @property {ActivityLoggerActions|string} Action 
 * @property {string} Url
 * @property {string} Timestamp ISO DataTime string
 * @property {BrowserInfo} BrowserInfo
 * @property {string} TargetElement
 * @property {string} TargetText
 * @property {string} TargetHtml
 */

/**
 * @interface BrowserInfo
 * @property {string} Language
 * @property {string} UserAgent
 * @property {boolean} Darkmode
 */