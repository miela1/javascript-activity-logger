/**
 * enables logging users activity on a page
 * @hideconstructor
 * @example <caption>Usage</caption>
 * ActivityLogger.BeginLogging(true);
 */
class ActivityLogger {
    /**
     * Adds the events the logger is listing on in order of enable logging of user activity
     * @private
     */
    Begin() {
        document.body.addEventListener("mousemove", this.mouseMove.bind(this));
        document.body.addEventListener("click", this.MouseClick.bind(this));
        window.addEventListener("load", this.PageLoad.bind(this));
        window.addEventListener("beforeunload", this.PageLeave.bind(this));
        window.addEventListener("resize", this.WindowResize.bind(this));

        this.mousePosition = { x: 0, y: 0 };
        this.windowSize = { width: window.innerWidth, height: window.innerHeight };
        this.screenSize = { width: window.screen.width, y: window.screen.height };
    }
    /**
     * 
     * @param {MouseEvent} event 
     */
    mouseMove(event) {
        let xPercent = Math.round((event.clientX / windowSize.width)*100);
        let yPercent = Math.round((event.clientY / windowSize.height)*100);
        this.mousePosition = {
            x: xPercent,
            y: yPercent
        }
    }
    /**
     * Called when window is resized, to get clients window size and screen reselution
     * @param {UIEvent} event 
     */
    WindowResize(event) {
        this.windowSize = {
            width: event.currentTarget.innerWidth,
            height: event.currentTarget.innerHeight
        }
        this.screenSize = {
            width: event.currentTarget.screen.width,
            height: event.currentTarget.screen.height
        } 
    }

    /**
     * Called when Mouse clicks
     * @private
     * @param {MouseEvent} event 
     */
    MouseClick(event) {
        var targetEl = event.target.tagName.toLowerCase();
        if (activityLoggerSettings.TargetElements.includes(targetEl)) {
            this.MakeLog(ActivityLoggerActions.MouseClick, event.target);
        }
    }
    /**
     * Is called when a page is loaded
     * @private
     * @param {Event} event 
     */
    PageLoad(event) {
        this.MakeLog(ActivityLoggerActions.PageLoad, null);
    }

    PageLeave(event) {
        this.MakeLog(ActivityLoggerActions.PageLeave, null)
    }

    /**
     * @private
     * @param {ActivityLoggerActions|string} action
     * @param {HTMLElement?} targetEl 
     */
    MakeLog(action, targetEl) {
        if (!activityLoggerSettings.loggingActions.includes(action))
            return;

        if (!window.clientInformation)
            window.clientInformation = navigator;

        var logData = {
            Action: action,
            Url: window.location.href,
            Timestamp: new Date().toISOString(),
            BrowserInfo: {
                Language: window.clientInformation.language,
                UserAgent: window.clientInformation.userAgent,
                Darkmode: false
            }
        }
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            logData.BrowserInfo.Darkmode = true;
        }
        if (targetEl) {
            logData.TargetElement = targetEl.tagName;
            logData.TargetSelect = this.GetElementIdentity(targetEl);
            logData.ScreenSize = this.clientScreen;
            logData.WindowSize = this.windowSize;
            logData.MousePosition = this.mousePosition;
        }
        switch (activityLoggerSettings.LoggingMode) {
            case ActivityLoggerModes.Console: this.WriteToConsole(logData); break;
            case ActivityLoggerModes.Url: this.WriteToServer(logData); break;
            case ActivityLoggerModes.ConsoleAndUrl: this.WriteToConsole(logData); this.WriteToServer(logData); break;
        }
    }
    /**
     * @private
     * @param {HTMLelement} el 
     */
    GetElementIdentity(el, stop) {
        var result = "";
        var classes = el.classList;
        if(!stop)
            stop = 0;
        stop++;
        
        if(el.id !== "") {
            result = el.id
        }
        classes.forEach(c => {
            if (typeof c === "string")
                result += " ." + c;
        });
        if(stop <= 5)
            result = `${this.GetElementIdentity(el.offsetParent,stop)} > ${result}`;
        return result;
    }

    /**
     * @private
     * @param {ActivityLogData} data 
     */
    WriteToConsole(data) {
        console.log(`%c ${data.Timestamp} - ActivityLogger`, "background: #42f566; color: #979c98", data);
    }
    /**
     * @private
     * @param {ActivityLogData} data 
     */
    WriteToServer(data) {
        var options = {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };
        try {
            fetch(activityLoggerSettings.LoggingServerUrl, options).then((response) => {
                response.json().then((json) => {
                    if (!json)
                        console.error("error in connection to server");
                });
            });
        } catch (ex) {
            console.error("connection error", ex);
        }

    }
    /**
     * Creates and starts a new logger
     * @static
     * @param {boolean} addGlobal If the logger is to be added as a global object at 'window.activityLogger'
     * @default false
     */
    static BeginLogging(addGlobal) {
        var logger = new ActivityLogger();
        logger.Begin();

        if (!addGlobal)
            addGlobal = false;
        if (addGlobal) {
            window.activityLogger = logger;
        }
        return logger;
    }
}