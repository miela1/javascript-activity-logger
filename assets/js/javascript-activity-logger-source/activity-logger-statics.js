/**
 * Static class with Log modes
 * @hideconstructor
 */
class ActivityLoggerModes {
    /**
     * Only log the activity to the clients JavaScript console
     * @readonly
     * @static
     */
    static get Console() { return "CONSOLE" };
    /**
     * Only log the activity the logging server
     * @readonly
     * @static
     */
    static get Url() { return "URL"; }
    /**
     * Log to both the Clients JavaScript Console and the logging server
     * @readonly
     * @static
     */
    static get ConsoleAndUrl() { return "CONSOLEANDURL" };
}
/**
 * Static class with Actions for the logging
 * @hideconstructor
 */
class ActivityLoggerActions {
    /**
     * Logging action for logging when a user clicks on an clickable elenemt
     * @readonly
     * @static
     */
    static get MouseClick() { return "MOUSECLICK" };
    /**
     * Logging action for when a page loades
     * @readonly
     * @static
     */
    static get PageLoad() { return "PAGELOAD" };
    /**
     * Logging action for when we leave a page
     * @readonly
     * @static
     */
    static get PageLeave() {return "PAGELEAVE" };
}