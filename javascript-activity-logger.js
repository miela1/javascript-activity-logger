/**
 * Static class with Log modes
 * @hideconstructor
 */
class ActivityLoggerModes {
    /**
     * Only log the activity to the clients JavaScript console
     * @readonly
     * @static
     */
    static get Console() { return "CONSOLE" };
    /**
     * Only log the activity the logging server
     * @readonly
     * @static
     */
    static get Url() { return "URL"; }
    /**
     * Log to both the Clients JavaScript Console and the logging server
     * @readonly
     * @static
     */
    static get ConsoleAndUrl() { return "CONSOLEANDURL" };
}
/**
 * Static class with Actions for the logging
 * @hideconstructor
 */
class ActivityLoggerActions {
    /**
     * Logging action for logging when a user clicks on an clickable elenemt
     * @readonly
     * @static
     */
    static get MouseClick() { return "MOUSECLICK" };
    /**
     * Logging action for when a page loades
     * @readonly
     * @static
     */
    static get PageLoad() { return "PAGELOAD" };
    /**
     * Logging action for when we leave a page
     * @readonly
     * @static
     */
    static get PageLeave() {return "PAGELEAVE" };
}
/**
 * @interface ActivityLogData
 * @property {ActivityLoggerActions|string} Action 
 * @property {string} Url
 * @property {string} Timestamp ISO DataTime string
 * @property {BrowserInfo} BrowserInfo
 * @property {string} TargetElement
 * @property {string} TargetText
 * @property {string} TargetHtml
 */

/**
 * @interface BrowserInfo
 * @property {string} Language
 * @property {string} UserAgent
 * @property {boolean} Darkmode
 */
/**
 * The settings for the Activity logger, the class will create a instance of it self and put it in `window.activityLoggerSettings`
 * The logger allows for dynamicly updating of its settings, the settings instance can therefor be updated or replaced with a new instance on the fly 
 * @property {ActivityLoggerModes|string} LoggingMode Gets or sets the log mode {ActivityLoggerModes}
 * @property {string[]} TargetElements gets or sets the target elemets an array of html element tagNames
 * @property {ActivityLoggerActions[]} LoggingActions Sets logging actions.
 * @property {string} LoggingServerUrl Gets or Sets the url to the logging server
 */
class ActivityLoggerSettings {
    constructor() {
        this.loggingMode = ActivityLoggerModes.Console;
        this.targetEls = ["a","button","input"];
        this.loggingActions = [
            ActivityLoggerActions.MouseClick,
            ActivityLoggerActions.PageLoad,
            ActivityLoggerActions.PageLeave]
    }
    /**
     * @ignore
     */
    set LoggingMode(value) {
        this.loggingMode = value;
    }
    /**
     * @ignore 
     */
    get LoggingMode() { return this.loggingMode; }

    /**
     * @ignore 
     */
    set TargetElements(value) {
        this.targetEls = value;
    }
    /**
     * @ignore
     */
    get TargetElements() {
        return this.targetEls;
    }
    /**
     * @ignore
     */
    set LoggingActions(value) {
        this.loggingActions = value;
    }

    /**
     * @ignore
     */
    set LoggingServerUrl(url) {
        this.loggingServer = url;
    }
    /**
     * @ignore
     */
    get LoggingServerUrl() {
        return this.loggingServer;
    }
    /**
     * Add Element to the logger 
     * @param {string} value the tagName of the elemet
     */
    AddTargetElement(value) {
        this.targetEls.push(value);
    }
    /**
     * removes elements from the logger, the elements of this type vill nolonger trigger an activity log
     * @param {string} value the tagName of the element 
     */
    RemoveTargetElement(value) {
        let index = this.targetEls.indexOf(value);
        delete this.targetEls[index];
    }
}

(() => {
    if (!window.activityLoggerSettings)
        window.activityLoggerSettings = new ActivityLoggerSettings();
})();

/**
 * enables logging users activity on a page
 * @hideconstructor
 * @example <caption>Usage</caption>
 * ActivityLogger.BeginLogging(true);
 */
class ActivityLogger {
    /**
     * Adds the events the logger is listing on in order of enable logging of user activity
     * @private
     */
    Begin() {
        document.body.addEventListener("mousemove", this.mouseMove.bind(this));
        document.body.addEventListener("click", this.MouseClick.bind(this));
        window.addEventListener("load", this.PageLoad.bind(this));
        window.addEventListener("beforeunload", this.PageLeave.bind(this));
        window.addEventListener("resize", this.WindowResize.bind(this));

        this.mousePosition = { x: 0, y: 0 };
        this.windowSize = { width: window.innerWidth, height: window.innerHeight };
        this.screenSize = { width: window.screen.width, y: window.screen.height };
    }
    /**
     * 
     * @param {MouseEvent} event 
     */
    mouseMove(event) {
        let xPercent = Math.round((event.clientX / windowSize.width)*100);
        let yPercent = Math.round((event.clientY / windowSize.height)*100);
        this.mousePosition = {
            x: xPercent,
            y: yPercent
        }
    }
    /**
     * Called when window is resized, to get clients window size and screen reselution
     * @param {UIEvent} event 
     */
    WindowResize(event) {
        this.windowSize = {
            width: event.currentTarget.innerWidth,
            height: event.currentTarget.innerHeight
        }
        this.screenSize = {
            width: event.currentTarget.screen.width,
            height: event.currentTarget.screen.height
        } 
    }

    /**
     * Called when Mouse clicks
     * @private
     * @param {MouseEvent} event 
     */
    MouseClick(event) {
        var targetEl = event.target.tagName.toLowerCase();
        if (activityLoggerSettings.TargetElements.includes(targetEl)) {
            this.MakeLog(ActivityLoggerActions.MouseClick, event.target);
        }
    }
    /**
     * Is called when a page is loaded
     * @private
     * @param {Event} event 
     */
    PageLoad(event) {
        this.MakeLog(ActivityLoggerActions.PageLoad, null);
    }

    PageLeave(event) {
        this.MakeLog(ActivityLoggerActions.PageLeave, null)
    }

    /**
     * @private
     * @param {ActivityLoggerActions|string} action
     * @param {HTMLElement?} targetEl 
     */
    MakeLog(action, targetEl) {
        if (!activityLoggerSettings.loggingActions.includes(action))
            return;

        if (!window.clientInformation)
            window.clientInformation = navigator;

        var logData = {
            Action: action,
            Url: window.location.href,
            Timestamp: new Date().toISOString(),
            BrowserInfo: {
                Language: window.clientInformation.language,
                UserAgent: window.clientInformation.userAgent,
                Darkmode: false
            }
        }
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            logData.BrowserInfo.Darkmode = true;
        }
        if (targetEl) {
            logData.TargetElement = targetEl.tagName;
            logData.TargetSelect = this.GetElementIdentity(targetEl);
            logData.ScreenSize = this.clientScreen;
            logData.WindowSize = this.windowSize;
            logData.MousePosition = this.mousePosition;
        }
        switch (activityLoggerSettings.LoggingMode) {
            case ActivityLoggerModes.Console: this.WriteToConsole(logData); break;
            case ActivityLoggerModes.Url: this.WriteToServer(logData); break;
            case ActivityLoggerModes.ConsoleAndUrl: this.WriteToConsole(logData); this.WriteToServer(logData); break;
        }
    }
    /**
     * @private
     * @param {HTMLelement} el 
     */
    GetElementIdentity(el, stop) {
        var result = "";
        var classes = el.classList;
        if(!stop)
            stop = 0;
        stop++;
        
        if(el.id !== "") {
            result = el.id
        }
        classes.forEach(c => {
            if (typeof c === "string")
                result += " ." + c;
        });
        if(stop <= 5)
            result = `${this.GetElementIdentity(el.offsetParent,stop)} > ${result}`;
        return result;
    }

    /**
     * @private
     * @param {ActivityLogData} data 
     */
    WriteToConsole(data) {
        console.log(`%c ${data.Timestamp} - ActivityLogger`, "background: #42f566; color: #979c98", data);
    }
    /**
     * @private
     * @param {ActivityLogData} data 
     */
    WriteToServer(data) {
        var options = {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };
        try {
            fetch(activityLoggerSettings.LoggingServerUrl, options).then((response) => {
                response.json().then((json) => {
                    if (!json)
                        console.error("error in connection to server");
                });
            });
        } catch (ex) {
            console.error("connection error", ex);
        }

    }
    /**
     * Creates and starts a new logger
     * @static
     * @param {boolean} addGlobal If the logger is to be added as a global object at 'window.activityLogger'
     * @default false
     */
    static BeginLogging(addGlobal) {
        var logger = new ActivityLogger();
        logger.Begin();

        if (!addGlobal)
            addGlobal = false;
        if (addGlobal) {
            window.activityLogger = logger;
        }
        return logger;
    }
}