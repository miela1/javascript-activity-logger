const { src, dest, parallel } = require('gulp');
const concat = require('gulp-concat');
var exec = require('child_process').exec;
const javascriptObfuscator = require('gulp-javascript-obfuscator');

function js() {
    var files = [
        "assets/js/javascript-activity-logger-source/activity-logger-statics.js",
        "assets/js/javascript-activity-logger-source/activity-logger-interfaces.js",
        "assets/js/javascript-activity-logger-source/activity-logger-settings.js",
        "assets/js/javascript-activity-logger-source/activity-logger-main.js"
    ]

    return src(files)
        .pipe(concat('javascript-activity-logger.js'))
        .pipe(dest("../javascript-activity-logger/"))
}
function doc(cb) {
    exec('jsdoc ./assets/js/javascript-activity-logger-source/', function (err) {
        cb(err);
    });
}
exports.default = parallel(js, doc);